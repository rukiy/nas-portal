import { createRouter, createWebHistory } from 'vue-router'
import Main from '../components/Main.vue'
import config from '../../public/config/config'



const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main,
    meta:{title: config.title}
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

router.afterEach((to) => {
  //遍历meta改变title
  if (to.meta.title) {
    document.title = to.meta.title;
  }
});

export default router
