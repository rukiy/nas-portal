# NasPortal

#### 介绍
简单的nas 首页导航!

采用vue3 重新开发.


编译
```bash
yarn
yarn serve
```

预览地址: https://nas-portal.vercel.app

效果图:![输入图片说明](screenshot/Snipaste_2022-02-14_14-17-53.gif)

![输入图片说明](screenshot/Snipaste_2022-03-14_15-07-37.png)


#### 使用说明

配置文件
public/config/config.json


```json
{
    "title": "Rukiy's Cloud Station",
    "description": "Nas Portal",
    "logo": "images/logo.svg",
    "footer": "<a target='_blank' href='https://gitee.com/rukiy/NasPortal'><img src='https://img.shields.io/badge/author-Rukiy-blue?style=flat-square' /></a>",
    "hitokoto": {
        "loadText": "正在加载一言...",
        "url": "https://v1.hitokoto.cn"
    },
    "groups": [
        {
            "name": "公网",
            "shorts": [
                {
                    "title": "Nas",
                    "desc": "RukiyStation",
                    "image": "images/shots/Setting.png",
                    "public": "https://www.baidu.com"
                },
                {
                    "title": "Note",
                    "desc": "个人笔记本",
                    "image": "images/shots/Note.png",
                    "public": "https://www.baidu.com"
                },
                {
                    "title": "Download",
                    "desc": "下载中心",
                    "image": "images/shots/Download.png",
                    "public": "https://www.baidu.com"
                }
            ]
        }
    ]
}
```
